const { connect, Experts } = require('./index');

const saveExpert = async (expert) => {
  const dbExpert = await Experts.findOne({
    fullName: expert.fullName
  });
  if (!dbExpert) {
    const newExpert = await Experts.create(expert);
    console.log(`created expert ${expert.fullName}`);
    return newExpert;
  }
  await Experts.findOneAndUpdate({ fullName: expert.fullName }, { $set: expert });
  console.log(`updated expert ${expert.fullName}`);
  return dbExpert;
};

const runSaveExpert = async () => {
  await connect('mongodb://techplays01:gGqj96VdSy9dUR4g@mongodb.techplays.com.br/?authSource=techplays01');
  await saveExpert({
    fullName: 'Elon Musk'
  });
};

runSaveExpert();
