const database = require('mongoose');

const strReq = {
	type: String,
	required: true
};

const schema = new database.Schema({
	title: strReq,
	description: String,
	sku: strReq,
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

schema.index({
  sku: 1
}, {
  unique: true
});

schema.index({
  title: 1
});

module.exports = database.model('Products', schema);
