const database = require('mongoose');

const strReq = {
	type: String,
	required: true
};

const schema = new database.Schema({
	fullName: strReq,
	document: strReq,
	createdAt: Date,
	updatedAt: Date
});

module.exports = database.model('People', schema);
