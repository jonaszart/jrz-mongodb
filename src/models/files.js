const database = require('mongoose');
const { typeEnums, fileExtensionEnums } = require('comac-enums');

const strReq = {
	type: String,
	required: true
};

const schema = new database.Schema({
	type: {
    type: String,
    enum: typeEnums
  },
	sku: strReq,
  extension: {
    type: String,
    enum: fileExtensionEnums
  },
  value: strReq
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

schema.index({
  sku: 1
});

schema.index({
  type: 1
});

module.exports = database.model('Files', schema);
