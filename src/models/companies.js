const database = require('mongoose');

const { countryEnum } = require('techplays-enum');

const strReq = {
	type: String,
	required: true
};

const schema = new database.Schema({
	fullName: strReq,
	document: strReq,
	alias: String,
	country: {
		type: String,
		enum: countryEnum
	},
	uf: String,
	founders: String,
	foundationYear: Date,
	summary: String,
	createdAt: Date,
	updatedAt: Date

});

module.exports = database.model('Companies', schema);
