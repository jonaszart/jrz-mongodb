const Companies = require('./companies');
const Files = require('./files');
const People = require('./people');
const Products = require('./products');
const Sellers = require('./sellers');

module.exports = {
	Companies,
	Files,
	People,
	Products,
	Sellers
};
