const database = require('mongoose');

const strReq = {
	type: String,
	required: true
};

const schema = new database.Schema({
	fullName: strReq,
	document: strReq,
	email: strReq,
	whats: strReq,
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

schema.index({
  document: 1
}, {
  unique: true
});

schema.index({
  email: 1
}, {
  unique: true
});

schema.index({
  whats: 1
}, {
  unique: true
});

module.exports = database.model('Sellers', schema);
