const assert = require('assert');
const {
	connect,
	Companies,
	People
} = require('../index');

/* eslint-disable no-underscore-dangle */
describe('@ Testing insert operations', async () => {
	connect('mongodb://techplays01:gGqj96VdSy9dUR4g@mongo71-farm76.kinghost.net:27017/techplays01');

  it('# should insert Company', async () => {
    const data = {
			fullName: 'Google',
			country: 'United States',
			founders: 'Sergey Brin and Larry Page',
      document: '987654'
    };
    const insert = await Companies.create(data);
    console.log(insert);
    assert.equal(true, typeof insert._id !== 'undefined');
    // await Companies.deleteOne({ _id: insert._id });
  });

  it('# should insert People', async () => {
    const data = {
			fullName: 'Elon Musk',
			document: '123456'
    };
    const insert = await People.create(data);
    console.log(insert);
    assert.equal(true, typeof insert._id !== 'undefined');
  });
});
