# jrz-mongodb

Módulo com os modelos `mongoose.model` para manipular as coleções do banco `techplays`.

## Instalação

```
npm install jrz-mongodb
```

## Modelos
| Nome | Finalidade |
| --- | --- |
| Experts | Techplays: Experts |

## Usando o modelo

Os modelos exportados por este módulo são modelos padrão do Mongoose. Mais detalhes na documentação do Mongoose. A `promiseLibrary` está setada como a Promise nativa.