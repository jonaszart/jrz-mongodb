const models = require('./src/models');
const connect = require('./src/connect');

module.exports = {
  connect,
  ...models
};
